﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwitchWithTime : MonoBehaviour
{

    public Material m_Selected;
    public float randomness = 6;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_Selected.color = NewDeltaColor(m_Selected.color);


    }

    private Color NewDeltaColor(Color color)
    {
        color.r += Time.deltaTime * UnityEngine.Random.Range(-randomness, randomness);
        color.g += Time.deltaTime * UnityEngine.Random.Range(-randomness, randomness);
        color.b += Time.deltaTime * UnityEngine.Random.Range(-randomness, randomness);
        color.r = Mathf.Clamp01(color.r);
        color.g = Mathf.Clamp01(color.g);
        return color;

    }
}
