﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

static class Toolbox
{
	static System.Random _random = new System.Random();


	/// <summary>
	/// Mélange un tableau de strings.
	/// </summary>
	/// <param name="arr">Le tableau de strings à mélanger.</param>
	/// <returns>Un nouveau tableau de strings.</returns>
	public static string[] RandomizeStrings(string[] arr)
	{
		List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();
		// Add all strings from array
		// Add new random int each time
		foreach (string s in arr)
		{
			list.Add(new KeyValuePair<int, string>(_random.Next(), s));
		}
		// Sort the list by the random number
		var sorted = from item in list
					 orderby item.Key
					 select item;
		// Allocate new string array
		string[] result = new string[arr.Length];
		// Copy values to array
		int index = 0;
		foreach (KeyValuePair<int, string> pair in sorted)
		{
			result[index] = pair.Value;
			index++;
		}
		// Return copied array
		return result;
	}


	/// <summary>
	/// Mélange une liste de générique.
	/// </summary>
	/// <typeparam name="T">Le type du générique.</typeparam>
	/// <param name="list">La liste générique à mélanger.</param>
	public static void RandomizeList<T>(this IList<T> list)
	{
		RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
		int n = list.Count;
		while (n > 1)
		{
			byte[] box = new byte[1];
			do provider.GetBytes(box);
			while (!(box[0] < n * (Byte.MaxValue / n)));
			int k = (box[0] % n);
			n--;
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}


	/// <summary>
	/// Search a GameObject in T.
	/// Create it AND set T as parent if search failed.
	/// </summary>
	/// <param name="T">Parent Transform</param>
	/// <param name="name">Name of the GameObject to search</param>
	/// <returns></returns>
	public static GameObject FindGameObject(Transform T, string name)
	{
		Transform searchedTransform = T.Find(name);
		GameObject GO;
		if (searchedTransform != null)
		{
			GO = searchedTransform.gameObject;
		}
		else
		{
			GO = new GameObject(name);
			GO.transform.parent = T.transform;
		}
		return GO;
	}
}
