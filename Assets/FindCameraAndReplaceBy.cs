﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindCameraAndReplaceBy : MonoBehaviour
{
    public RenderTexture m_renderTexture;
    
    private void OnLevelWasLoaded(int level)
    {
        if(Camera.main!=null)
        Camera.main.targetTexture = m_renderTexture;
    }
}
